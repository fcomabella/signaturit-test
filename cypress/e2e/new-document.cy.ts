import makeServer from '../../src/server';

describe('New document form', () => {
  let server;
  beforeEach(() => {
    server = makeServer({ environment: 'test' });
    cy.visit('/documents/new');
  });

  afterEach(() => {
    server.shutdown();
  });

  it('Should start with the simple document type', () => {
    cy.get('select').should('have.value', 'simple');

    cy.get('[type=submit]')
      .should('be.visible')
      .and('have.text', 'Save Document');

    cy.get('a[class*=ButtonLink]')
      .should('be.visible')
      .and('have.text', 'Cancel & Return');

    cy.get('input[name=date]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'date');

    cy.get('input[name=title]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'text');

    cy.get('textarea[name=text]').should('not.exist');

    cy.get('input[name=images]').should('not.exist');
  });

  it('Should change to the Custom document type', () => {
    cy.get('select').select('custom');

    cy.get('[type=submit]')
      .should('be.visible')
      .and('have.text', 'Save Document');

    cy.get('a[class*=ButtonLink]')
      .should('be.visible')
      .and('have.text', 'Cancel & Return');

    cy.get('input[name=date]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'date');

    cy.get('input[name=title]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'text');

    cy.get('textarea[name=text]').should('be.visible');

    cy.get('input[name=images]').should('not.exist');
  });

  it('Should change to the Advanced document type', () => {
    cy.get('select').select('advanced');

    cy.get('[type=submit]')
      .should('be.visible')
      .and('have.text', 'Save Document');

    cy.get('a[class*=ButtonLink]')
      .should('be.visible')
      .and('have.text', 'Cancel & Return');

    cy.get('input[name=date]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'date');

    cy.get('input[name=title]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'text');

    cy.get('textarea[name=text]').should('be.visible');

    cy.get('input[name=images]')
      .should('be.visible')
      .invoke('attr', 'type')
      .should('equal', 'file');
  });
});
