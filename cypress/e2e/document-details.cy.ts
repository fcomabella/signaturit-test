import { DateTime } from 'luxon';

import makeServer from '../../src/server';

describe('Document details', () => {
  let server;
  beforeEach(() => {
    server = makeServer({ environment: 'test' });
  });

  afterEach(() => {
    server.shutdown();
  });

  it('Links with only one document', () => {
    server.createList('document', 1);

    cy.visit('/documents/0');

    cy.get('[class*=DocumentsListLink] a')
      .should('have.attr', 'href')
      .and('equal', '/');

    cy.get('[class*=PrevDocumentLink] a').should('not.exist');
    cy.get('[class*=NextDocumentLink] a').should('not.exist');
  });

  it('Links for the first document', () => {
    server.createList('document', 2);

    cy.visit('/documents/0');

    cy.get('[class*=DocumentsListLink] a')
      .should('have.attr', 'href')
      .and('equal', '/');

    cy.get('[class*=PrevDocumentLink] a').should('not.exist');
    cy.get('[class*=NextDocumentLink] a')
      .should('have.attr', 'href')
      .and('equal', '/documents/1');
  });

  it('Links for the last document', () => {
    server.createList('document', 2);

    cy.visit('/documents/1');

    cy.get('[class*=DocumentsListLink] a')
      .should('have.attr', 'href')
      .and('equal', '/');

    cy.get('[class*=PrevDocumentLink] a')
      .should('have.attr', 'href')
      .and('equal', '/documents/0');
    cy.get('[class*=NextDocumentLink] a').should('not.exist');
  });

  it('Links for the other documents', () => {
    server.createList('document', 3);

    cy.visit('/documents/1');

    cy.get('[class*=DocumentsListLink] a')
      .should('have.attr', 'href')
      .and('equal', '/');

    cy.get('[class*=PrevDocumentLink] a')
      .should('have.attr', 'href')
      .and('equal', '/documents/0');
    cy.get('[class*=NextDocumentLink] a')
      .should('have.attr', 'href')
      .and('equal', '/documents/2');
  });

  it('Should show a Delete button', () => {
    server.create('document');

    cy.visit('/documents/0');

    cy.get('button[type=button]').should('have.text', 'Delete');
  });

  it('A simple document only has Title and Date', () => {
    const doc = server.create('document', {
      text: undefined,
      image: undefined,
    });

    cy.visit('/documents/0');

    cy.get('h1').should('have.text', doc.title);

    cy.get('[class*=DateContainer] span:first-child').should(
      'have.text',
      DateTime.fromISO(doc.date).toLocaleString()
    );
  });

  it('A Custom document also has a Text', () => {
    const doc = server.create('document', {
      text: 'This is a custom document text',
      image: undefined,
    });

    cy.visit('/documents/0');

    cy.get('[class*=Pre]').should('have.text', doc.text);
  });

  it('An Advanced document has a Text and an image', () => {
    const doc = server.create('document', {
      text: 'This is an advanced document text',
      image:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAFUlEQVR42mNkYPhfz0AEYBxVSF+FAP5FDvcfRYWgAAAAAElFTkSuQmCC',
    });

    cy.visit('/documents/0');

    cy.get('[class*=Pre]').should('have.text', doc.text);

    cy.get('[class*=ImageContainer]')
      .should('have.attr', 'src')
      .and('equal', doc.image);
  });

  it('Shows the loading state', () => {
    server.timing = 100;
    server.createList('document', 1);

    cy.visit('/documents/0');
    cy.contains('Loading the document').should('be.visible');

    cy.contains('Loading the document').should('not.exist');
  });

  it('Deletes the document', () => {
    server.timing = 100;
    server.createList('document', 1);

    cy.visit('/documents/0');

    cy.get('button[type=button]').click();

    cy.contains('Deleting the document').should('be.visible');
    cy.url().should('not.contain', '/documents/');
    cy.contains('There are no documents').should('be.visible');
  });
});
