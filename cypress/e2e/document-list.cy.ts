import { DateTime } from 'luxon';

import { rangedRandom } from '../../src/lib/random';
import makeServer from '../../src/server';

describe('Document list', () => {
  let server;
  beforeEach(() => {
    server = makeServer({ environment: 'test' });
  });

  afterEach(() => {
    server.shutdown();
  });

  it('Shows the loading state', () => {
    server.timing = 100;

    cy.visit('/');
    cy.contains('Refreshing list...').should('be.visible');
    cy.contains('Refreshing list...').should('not.exist');
  });

  it('Shows an empty document list', () => {
    cy.visit('/');
    cy.get('select').should('have.value', 'all');
    cy.contains('There are no documents');
    cy.contains('New Document');
  });

  ['simple', 'custom', 'advanced', 'all'].forEach((filterValue) => {
    it(`Changes the filter query param to ${filterValue}`, () => {
      if (filterValue === 'all') {
        cy.get('select').select('simple');
      }
      cy.get('select').select(filterValue);
      cy.url().should('include', `filter=${filterValue}`);
    });
  });

  it('Shows the documents', () => {
    const docCount = rangedRandom(0, 10);
    server.createList('document', docCount);

    cy.visit('/');

    cy.get('[class*=DocumentListLine]').should('have.length', docCount);
    cy.get('[class*=PaginationLink]').should('not.exist');
  });

  it('paginates the documents', () => {
    const docCount = rangedRandom(11, 60);
    const pageCount = Math.ceil(docCount / 10);
    server.createList('document', docCount);

    cy.visit('/');

    cy.get('[class*=PaginationLink]').should('have.length.at.least', pageCount);
  });

  it(`Shows the document type, title and link for Simple documents`, () => {
    const doc = {
      id: '0',
      title: 'Simple document',
      date: DateTime.now().toISODate(),
      text: undefined,
      image: undefined,
    };

    server.create('document', doc);

    cy.visit('/');

    cy.get('[class*=DocumentListTitle]').as('title');

    cy.get('@title').find(':first-child').should('have.text', doc.title);

    cy.get('@title').find(':nth-child(2)').should('contain.text', 'Simple');

    cy.get('[class*=DocumentListLink]')
      .should('have.attr', 'href')
      .should('not.be.empty')
      .and('contain', `/documents/${doc.id}`);
  });

  it(`Shows the document type, title and link for Custom documents`, () => {
    const doc = {
      id: '0',
      title: 'Custom document',
      date: DateTime.now().toISODate(),
      text: 'Custom document text',
      image: undefined,
    };

    server.create('document', doc);

    cy.visit('/');

    cy.get('[class*=DocumentListTitle]').as('title');

    cy.get('@title').find(':first-child').should('have.text', doc.title);

    cy.get('@title').find(':nth-child(2)').should('contain.text', 'Custom');

    cy.get('[class*=DocumentListLink]')
      .should('have.attr', 'href')
      .should('not.be.empty')
      .and('contain', `/documents/${doc.id}`);
  });

  it(`Shows the document type, title and link for Advanced documents`, () => {
    const doc = {
      id: '0',
      title: 'Custom document',
      date: DateTime.now().toISODate(),
      text: 'Custom document text',
      image:
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAFUlEQVR42mNkYPhfz0AEYBxVSF+FAP5FDvcfRYWgAAAAAElFTkSuQmCC',
    };

    server.create('document', doc);

    cy.visit('/');

    cy.get('[class*=DocumentListTitle]').as('title');

    cy.get('@title').find(':first-child').should('have.text', doc.title);

    cy.get('@title').find(':nth-child(2)').should('contain.text', 'Advanced');

    cy.get('[class*=DocumentListLink]')
      .should('have.attr', 'href')
      .should('not.be.empty')
      .and('contain', `/documents/${doc.id}`);
  });
});
