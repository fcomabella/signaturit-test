import { DateTime } from 'luxon';
import { createServer, Factory, Model, Response } from 'miragejs';

import {
    IDocument, isAdvancedDocument, isCustomDocument, isSimpleDocument
} from './interfaces/document';
import { rangedRandom } from './lib/random';
import { TDocumentType } from './types/document-type';

// eslint-disable-next-line no-unused-vars
const filterFunctions: Record<TDocumentType, (document: IDocument) => boolean> =
  {
    all: () => true,
    advanced: isAdvancedDocument,
    custom: isCustomDocument,
    simple: isSimpleDocument,
  };

export default function makeServer({ environment = 'development' } = {}) {
  const server = createServer({
    environment,
    models: {
      document: Model.extend<Partial<IDocument>>({}),
    },
    factories: {
      document: Factory.extend<IDocument>({
        id(i) {
          return i.toString();
        },
        title(i) {
          return `Document ${i}`;
        },
        date() {
          return DateTime.now().toISO();
        },
        text(i) {
          if (rangedRandom(1, 3) === 3) {
            return `Document ${i} text.`;
          }
          return undefined;
        },
        image() {
          if (this.text && rangedRandom(1, 3) === 3) {
            return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAYAAABWKLW/AAAAEklEQVR42mP8z8AARBDAiJMDAIzoBf5Dp2P2AAAAAElFTkSuQmCC';
          }
          return undefined;
        },
      }),
    },
    seeds(server) {
      const docCount = rangedRandom(11, 33);
      server.createList('document', docCount);
    },
    routes() {
      this.urlPrefix = 'api';
      this.get('/documents', (schema, request) => {
        let offset = parseInt(request.queryParams?.offset ?? '0', 10);

        if (isNaN(offset)) {
          offset = 0;
        }

        let limit = parseInt(request.queryParams?.limit ?? '10', 10);

        if (isNaN(limit)) {
          limit = 10;
        }

        const filter: TDocumentType = request.queryParams?.filter ?? 'all';

        const documents: IDocument[] = schema
          .all('document')
          .models.filter(filterFunctions[filter]);

        return {
          documents: documents.slice(offset, offset + limit),
          count: documents.length,
        };
      });

      this.get('/documents/:id', (schema, request) => {
        const requestedId = request.params.id;
        const documents = schema.all('document').models;

        const documentIndex = documents.findIndex(
          ({ id }) => id === requestedId
        );

        if (documentIndex === -1) {
          return new Response(404);
        }

        const prevDocumentId: string | undefined =
          documents[documentIndex - 1]?.id;
        const nextDocumentId: string | undefined =
          documents[documentIndex + 1]?.id;

        return {
          document: documents[documentIndex],
          prevDocumentId,
          nextDocumentId,
        };
      });

      this.delete('/documents/:id', (schema, request) => {
        const requestedId = request.params.id;
        const document = schema.find('document', requestedId);

        if (document === null) {
          return new Response(404);
        }

        document.destroy();

        return {};
      });

      // eslint-disable-next-line no-unused-vars
      this.post('/documents', (schema, request) => {
        const document = JSON.parse(request.requestBody);

        return schema.create('document', document).attrs;
      });
    },
  });

  return server;
}
