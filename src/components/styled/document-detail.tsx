import { DateTime } from 'luxon';
import React from 'react';
import styled from 'styled-components/macro';

import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

interface IDateContainerProps {
  className?: string;
  date: string;
  icon: IconProp;
}
export const DateContainer = styled(
  ({ className, date, icon }: IDateContainerProps) => (
    <span className={className}>
      <span>{DateTime.fromISO(date).toLocaleString()}</span>
      <span>
        <FontAwesomeIcon icon={icon} />
      </span>
    </span>
  )
)`
  & > span {
    border: 1px solid #8c8c8c;
    padding: 0.25em;

    :first-child {
      border-right: none;
      background-color: #e6e6e6;
      border-top-left-radius: 4px;
      border-bottom-left-radius: 4px;
    }

    :last-child {
      background-color: #cccccc;
      border-top-right-radius: 4px;
      border-bottom-right-radius: 4px;
    }
  }
`;

export const ImageContainer = styled.img`
  max-height: 50vh;
  max-width: 50vh;
  object-fit: scale-down;
`;

export const HeaderBar = styled.div`
  display: grid;
  grid-template-columns: 15em 1fr 15em;

  div {
    padding: 1em 0;
  }
`;

export const PrevDocumentLink = styled.div`
  grid-column: 1;
  text-align: left;
`;

export const DocumentsListLink = styled.div`
  grid-column: 2;
  text-align: center;
`;

export const NextDocumentLink = styled.div`
  grid-column: 3;
  text-align: right;
`;
