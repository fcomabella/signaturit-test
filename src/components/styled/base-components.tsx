import React from 'react';
import { Link, LinkProps } from 'react-router-dom';
import styled, { css, DefaultTheme } from 'styled-components/macro';

type Sizes = 'small' | 'normal' | 'big' | 'none';
type SizeUnits = 'em' | 'rem';
const mapSizeToCss = (size: Sizes, unit: SizeUnits = 'em') => {
  switch (size) {
    case 'big':
      return `1.25${unit}`;
    case 'normal':
      return `1${unit}`;
    case 'small':
      return `0.75${unit}`;
    case 'none':
      return '0';
  }
};

interface IMarginProps {
  margin?: Sizes;
}
interface IParagraphProps extends IMarginProps {
  align?: 'left' | 'right' | 'justify' | 'center';
  size?: Sizes;
}
export const Paragraph = styled.p<IParagraphProps>`
  ${({ align = 'left', size = 'normal', margin = 'normal' }) => css`
    text-align: ${align};
    font-size: ${mapSizeToCss(size)};
    margin: ${mapSizeToCss(margin, 'rem')};
  `}
`;

interface IButtonProps {
  theme: DefaultTheme;
  subTheme?: keyof DefaultTheme['button']['colors'];
}
const buttonCss = ({ theme, subTheme = 'default' }: IButtonProps) => {
  const { font, background, border } = theme.button.colors[subTheme];
  return css`
    color: ${font};
    background-color: ${background};
    border: 1px solid ${border};
    border-radius: ${theme.button.borderRadius};
  `;
};

export const Button = styled.button<IButtonProps>`
  margin: 0 1em;
  padding: 0.25em 1em;
  cursor: pointer;
  ${buttonCss}
`;

const CenteredContainer = styled.div`
  padding: 2rem 0;
  display: grid;
  justify-content: center;
  min-height: 100vh;
  width: 100vw;
`;

interface ICenteredContentProps {
  className?: string;
  children: React.ReactNode;
}

export const CenteredContent = styled(
  ({ className, children }: ICenteredContentProps) => (
    <CenteredContainer>
      <div className={className}>{children}</div>
    </CenteredContainer>
  )
)`
  min-width: 50vw;
  max-width: 90vw;
`;

export const Pre = styled.pre`
  white-space: pre-wrap;
`;

interface IFlexContainerProps {
  direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
  flexWrap?: 'nowrap' | 'wrap' | 'wrap-reverse';
  justifyContent?:
    | 'flex-start'
    | 'flex-end'
    | 'center'
    | 'space-between'
    | 'space-around'
    | 'space-evenly';
  alignItems?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline';
  gap?: number;
}
export const FlexContainer = styled.div<IFlexContainerProps>`
  display: flex;
  ${({
    direction = 'row',
    flexWrap = 'nowrap',
    justifyContent = 'flex-start',
    alignItems = 'stretch',
    gap = 0,
  }) => css`
    flex-direction: ${direction};
    flex-wrap: ${flexWrap};
    justify-content: ${justifyContent};
    align-items: ${alignItems};
    gap: ${gap}rem;
  `};
`;

interface IFlexChildProps {
  order?: number;
  grow?: number;
  shrink?: number;
  basis?: string;
}
export const FlexChild = styled.div<IFlexChildProps>`
  ${({ order = 0, grow = 0, shrink = 1, basis = 'auto' }) => css`
    order: ${order};
    flex-grow: ${grow};
    flex-shrink: ${shrink};
    flex-basis: ${basis};
  `}
`;
interface IButtonLinkProps extends LinkProps, IButtonProps {
  className?: string;
  children: React.ReactNode;
}
export const ButtonLink = styled(
  ({ className, children, to }: IButtonLinkProps) => (
    <Link to={to} className={className}>
      {children}
    </Link>
  )
)`
  ${buttonCss}
  padding: 0.25em 0.5em;
  text-decoration: none;
  cursor: pointer;
  &:hover {
    text-decoration: underline;
  }
`;

export const Select = styled.select`
  padding: 0.5em;
  border-radius: 0.25em;
  background-color: transparent;
`;
