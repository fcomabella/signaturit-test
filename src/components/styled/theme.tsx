import { DefaultTheme } from 'styled-components/macro';

export const theme: DefaultTheme = {
  button: {
    borderRadius: '4px',
    colors: {
      default: {
        background: 'white',
        border: '#e6e6e6',
        font: 'black',
      },
      warning: {
        background: '#ffc107',
        border: '#e6ac00',
        font: 'black',
      },
      danger: {
        background: '#dc3545',
        border: '#c32232',
        font: 'white',
      },
      success: {
        background: '#198754',
        border: '#105635',
        font: 'white',
      },
    },
  },
};
