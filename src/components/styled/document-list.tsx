import React from 'react';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components/macro';

import { IDocument } from '../../interfaces/document';
import { ButtonLink, FlexChild, FlexContainer } from './base-components';

export const DocumentListLine = styled(({ children, className }) => (
  <FlexContainer
    alignItems="center"
    justifyContent="flex-end"
    flexWrap="wrap"
    className={className}
  >
    {children}
  </FlexContainer>
))`
  padding-top: 0.5em;
  padding-bottom: 0.5em;
  margin-top: 1em;
  margin-bottom: 1em;
  background: transparent;
  border: 2px solid #333;
  border-radius: 0.5em;
`;

export const DocumentListImageContainer = styled(({ children, className }) => (
  <FlexChild basis="10em" shrink={0} className={className}>
    {children}
  </FlexChild>
))`
  position: relative;
  text-align: center;
`;

export const DocumentListImage = styled.img`
  max-width: 100%;
  max-height: 10em;
  object-fit: scale-down;
`;

export const DocumentListTitle = styled.div`
  flex-grow: 1;
`;

const DocumentLinkContainer = styled(({ children, className }) => (
  <FlexChild shrink={0} className={className}>
    {children}
  </FlexChild>
))`
  padding: 0 1em;
`;

interface IDocumentListLinkProps {
  className?: string;
  document: IDocument;
}

export const DocumentListLink = styled(
  ({ className, document }: IDocumentListLinkProps) => (
    <DocumentLinkContainer>
      <ButtonLink
        to={`/documents/${document.id}`}
        className={className}
        subTheme="warning"
      >
        Detail
      </ButtonLink>
    </DocumentLinkContainer>
  )
)`
  box-shadow: 0.25em 0.25em 2px #aaa;
`;

export const PaginationLink = styled(({ className, children, to }) => (
  <Link className={className} to={to}>
    {children}
  </Link>
))`
  padding: 0.25em;
  margin: 0.25em;
  text-decoration: none;
  color: black;
  ${({ isActive }) =>
    isActive &&
    css`
      color: red;
      text-decoration: underline;
    `}
  &:hover {
    text-decoration: underline;
  }
`;
