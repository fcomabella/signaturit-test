import 'styled-components';

type ButtonTypes = 'default' | 'warning' | 'danger' | 'success';
interface ButtonColors {
  background: string;
  border: string;
  font: string;
}
interface ButtonDefinition {
  borderRadius: string;
  colors: Record<ButtonTypes, ButtonColors>;
}
declare module 'styled-components' {
  export interface DefaultTheme {
    button: ButtonDefinition;
  }
}
