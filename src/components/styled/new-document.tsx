import React from 'react';
import { FieldError, useFormContext } from 'react-hook-form';
import styled, { css } from 'styled-components/macro';

import { FlexContainer } from './base-components';

const ErrorCaption = styled.div`
  color: red;
`;

interface IFormFieldProps {
  className?: string;
  fieldName: string;
  error?: FieldError;
  required?: boolean;
  type?: React.HTMLInputTypeAttribute;
  children: React.ReactNode;
}
const FormField = styled(
  ({ className, fieldName, error, children }: IFormFieldProps) => {
    return (
      <div className={className}>
        <label>{fieldName}:</label>
        <div> {children} </div>
        {error?.type === 'required' && (
          <ErrorCaption>This field is required</ErrorCaption>
        )}
      </div>
    );
  }
)`
  label {
    display: block;
    margin-top: 1em;
    margin-bottom: 0.5em;
    &:first-letter {
      text-transform: uppercase;
    }
    ${({ required }) =>
      required
        ? css`
            &::after {
              content: '*';
            }
          `
        : ''}
    ${({ error }) =>
      error?.type === 'required'
        ? css`
            color: red;
          `
        : ''}
  }
`;

interface IInputfieldProps extends Omit<IFormFieldProps, 'children'> {
  type?: React.HTMLInputTypeAttribute | 'textarea';
}
export const InputField = styled(
  ({ className, type = 'text', fieldName, required }: IInputfieldProps) => {
    const {
      register,
      formState: { errors },
    } = useFormContext();

    return (
      <FormField
        fieldName={fieldName}
        required={required}
        error={errors[fieldName]}
      >
        {type !== 'textarea' && (
          <input
            className={className}
            type={type}
            accept="image/jpeg,image/png"
            {...register(fieldName, { required })}
          />
        )}
        {type === 'textarea' && (
          <textarea
            className={className}
            {...register(fieldName, { required })}
            rows={10}
          ></textarea>
        )}
      </FormField>
    );
  }
)`
  width: 100%;
  outline: none;
  border: 1px solid #000;
  border-radius: 0.25em;
  padding: 0.5em;
  ${({ error }) =>
    error?.type === 'required'
      ? css`
          border-color: red;
        `
      : ''}
`;

export const SelectField = styled(
  ({ className, fieldName, required, children }: IFormFieldProps) => {
    const {
      register,
      formState: { errors },
    } = useFormContext();

    return (
      <FormField
        fieldName={fieldName}
        required={required}
        error={errors[fieldName]}
      >
        <select className={className} {...register(fieldName, { required })}>
          {children}
        </select>
      </FormField>
    );
  }
)`
  width: 100%;
  border: 1px solid black;
  border-radius: 0.25em;
  padding: 0.5em;
  background-color: transparent;
`;

export const BottomBar = styled(({ className, children }) => (
  <FlexContainer className={className}>{children}</FlexContainer>
))`
  margin: 1rem 0;
`;
