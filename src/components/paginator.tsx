import React from 'react';

import { IDocumentsQuery } from '../interfaces/documents-query';
import { Paragraph } from './styled/base-components';
import { PaginationLink } from './styled/document-list';

interface IPaginatorProps extends IDocumentsQuery {
  total: number;
  baseUrl?: string;
}

const serializeParams = (params: Partial<IDocumentsQuery>) =>
  Object.entries(params)
    .filter(([key, value]) => {
      switch (key) {
        case 'filter':
          return value !== 'all';
        case 'limit':
          return value !== 10;
        case 'offset':
          return !!value;
        default:
          return true;
      }
    })
    .map((entry) => entry.join('='))
    .join('&');

export default function Paginator({
  offset,
  limit,
  filter,
  total,
}: // baseUrl = '/',
IPaginatorProps) {
  const pageCount = Math.ceil(total / limit);

  if (pageCount === 1) {
    return null;
  }

  const currentPage = offset / limit + 1;

  const pages = new Array(11)
    .fill(undefined)
    .map((_, index) => currentPage + index - 5)
    .filter((page) => page > 0 && page <= pageCount);

  return (
    <Paragraph align="center" size="big">
      <>
        {currentPage > 1 && (
          <>
            <PaginationLink
              key="first"
              to={`/?${serializeParams({ filter, limit })}`}
            >
              {'<<'}
            </PaginationLink>{' '}
            <PaginationLink
              key="prev"
              to={`/?${serializeParams({
                filter,
                limit,
                offset: (offset / limit - 1) * limit,
              })}`}
            >
              {'<'}
            </PaginationLink>
          </>
        )}{' '}
        {pages.map((page) => (
          <PaginationLink
            isActive={page === currentPage}
            key={page}
            to={`/?${serializeParams({
              filter,
              limit,
              offset: (page - 1) * limit,
            })}`}
          >
            {page}
          </PaginationLink>
        ))}
        {currentPage < pageCount && (
          <>
            <PaginationLink
              key="next"
              to={`/?${serializeParams({
                filter,
                limit,
                offset: (offset / limit + 1) * limit,
              })}`}
            >
              {'>'}
            </PaginationLink>{' '}
            <PaginationLink
              key="last"
              to={`/?${serializeParams({
                filter,
                limit,
                offset: (pageCount - 1) * limit,
              })}`}
            >
              {'>>'}
            </PaginationLink>
          </>
        )}
      </>
    </Paragraph>
  );
}
