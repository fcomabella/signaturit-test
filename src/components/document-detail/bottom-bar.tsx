import React from 'react';

import { faCalendarDays } from '@fortawesome/free-solid-svg-icons';

import { IDocument } from '../../interfaces/document';
import { Button, FlexChild, FlexContainer } from '../styled/base-components';
import { DateContainer } from '../styled/document-detail';

interface IDocumentDetailBottomBarProps {
  document: IDocument;
  // eslint-disable-next-line no-unused-vars
  deleteButtonOnClick: (id: string) => void;
}
export default function DocumentDetailBottomBar({
  document: { date, id },
  deleteButtonOnClick,
}: IDocumentDetailBottomBarProps) {
  return (
    <FlexContainer alignItems="center">
      <FlexChild grow={1}>
        <DateContainer date={date} icon={faCalendarDays} />
      </FlexChild>
      <FlexChild grow={0}>
        <Button
          subTheme="danger"
          type="button"
          onClick={(e) => {
            e.preventDefault();
            deleteButtonOnClick(id);
          }}
        >
          Delete
        </Button>
      </FlexChild>
    </FlexContainer>
  );
}
