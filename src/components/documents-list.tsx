import React from 'react';

import {
    IDocument, isAdvancedDocument, isCustomDocument, isSimpleDocument
} from '../interfaces/document';
import { Paragraph } from './styled/base-components';
import {
    DocumentListImage, DocumentListImageContainer, DocumentListLine, DocumentListLink,
    DocumentListTitle
} from './styled/document-list';

export default function DocumentsList({
  documents,
}: {
  documents: IDocument[];
}) {
  if (documents.length === 0) {
    return <Paragraph>There are no documents</Paragraph>;
  }

  return (
    <>
      {documents.map((document) => {
        const { id, image, title } = document;
        return (
          <DocumentListLine key={id} data-testid="test">
            <DocumentListImageContainer>
              {image && <DocumentListImage src={`data:${image}`} alt={title} />}
            </DocumentListImageContainer>
            <DocumentListTitle>
              <Paragraph size="big" margin="small">
                {title}
              </Paragraph>
              {isSimpleDocument(document) && (
                <Paragraph margin="small">Simple</Paragraph>
              )}
              {isCustomDocument(document) && (
                <Paragraph margin="small">Custom</Paragraph>
              )}
              {isAdvancedDocument(document) && (
                <Paragraph margin="small">Advanced</Paragraph>
              )}
            </DocumentListTitle>
            <DocumentListLink document={document} />
          </DocumentListLine>
        );
      })}
    </>
  );
}
