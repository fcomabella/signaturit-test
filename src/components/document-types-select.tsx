import React, { ChangeEvent } from 'react';

import { isTDocumentType, TDocumentType } from '../types/document-type';
import { Paragraph, Select } from './styled/base-components';

interface IDocumentTypesSelectProps {
  value: TDocumentType;
  // eslint-disable-next-line no-unused-vars
  onChange: (newValue: TDocumentType) => void;
}

export default function DocumentTypesSelect({
  value,
  onChange,
}: IDocumentTypesSelectProps) {
  const filterSelectOnChange = (e: ChangeEvent<HTMLSelectElement>) => {
    e.preventDefault();
    const newValue = e.currentTarget.value;
    if (isTDocumentType(newValue)) {
      onChange(newValue);
    }
  };

  return (
    <Paragraph align="right" margin="none">
      <Select onChange={filterSelectOnChange} value={value}>
        <option value="all">All</option>
        <option value="simple">Simple</option>
        <option value="custom">Custom</option>
        <option value="advanced">Advanced</option>
      </Select>
    </Paragraph>
  );
}
