import { IDocument } from './document';
import { IPaginatedResponse } from './paginated-response';

export interface IDocumentsResponse extends IPaginatedResponse {
  documents: IDocument[];
}
