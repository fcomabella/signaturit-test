import { TDocumentType } from '../types/document-type';
import { IPaginatedQuery } from './paginated-query';

export interface IDocumentsQuery extends IPaginatedQuery {
  filter?: TDocumentType;
}
