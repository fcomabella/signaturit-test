export interface IDocument {
  id: string;
  title: string;
  date: string;
  text?: string;
  image?: string;
}

export const isSimpleDocument = ({ text, image }: IDocument) => !text && !image;
export const isCustomDocument = ({ text, image }: IDocument) =>
  !!text && !image;
export const isAdvancedDocument = ({ image }: IDocument) => !!image;
