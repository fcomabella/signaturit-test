export interface IPaginatedQuery {
  offset: number;
  limit: number;
}
