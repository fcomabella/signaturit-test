export interface AsyncState {
  status: 'idle' | 'loading' | 'failed' | 'successful';
}