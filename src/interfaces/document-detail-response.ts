import { IDocument } from './document';

export interface IDocumentDetailResponse {
  document: IDocument;
  nextDocumentId?: string;
  prevDocumentId?: string;
}
