export const rangedRandom = (min = 0, max = 0, asInteger = true) => {
  const result = Math.random() * (max - min + 1) + min;

  return asInteger ? Math.floor(result) : result;
};
