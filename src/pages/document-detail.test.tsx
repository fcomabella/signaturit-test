import React from 'react';
import { Provider } from 'react-redux';

import { render } from '@testing-library/react';

import { store } from '../app/store';
import DocumentDetailPage from './document-detail';

jest.mock('react-router-dom', () => ({
  useParams() {
    return { id: '1' };
  },
}));

test('renders document id', () => {
  const i = 1;

  const { getByText } = render(
    <Provider store={store}>
      <DocumentDetailPage />
    </Provider>
  );

  expect(getByText(i)).toBeInTheDocument();
});
