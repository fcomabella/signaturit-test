import { Base64 } from 'js-base64';
import React, { useEffect } from 'react';
import { FormProvider, SubmitHandler, useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import {
    Button, ButtonLink, FlexChild, FlexContainer, Paragraph
} from '../components/styled/base-components';
import { InputField, SelectField } from '../components/styled/new-document';
import { IDocument } from '../interfaces/document';
import { useAddDocumentMutation } from '../services/documents-api';

const fileToBase64 = async (file: File) => {
  return Base64.fromUint8Array(new Uint8Array(await file.arrayBuffer()));
};

interface INewDocument extends Omit<IDocument, 'id' | 'image'> {
  images?: FileList;
  type: 'simple' | 'custom' | 'advanced';
}

export default function NewDocumentPage() {
  const methods = useForm<INewDocument>();
  const [
    postDocument,
    { isLoading: isSaving, isSuccess: saveSuccess, data: newDocument },
  ] = useAddDocumentMutation();
  const navigate = useNavigate();
  const formOnSubmit: SubmitHandler<INewDocument> = async ({
    date,
    title,
    text,
    images,
  }) => {
    let image: string | undefined = undefined;

    if (images && images.length > 0) {
      const imageFile = images[0];
      const imageData = await fileToBase64(imageFile);
      image = `data:${imageFile.type};base64,${imageData}`;
    }

    postDocument({ date, title, text, image });
  };

  const watchType = methods.watch('type');

  useEffect(() => {
    if (saveSuccess && newDocument) {
      navigate(`/documents/${newDocument.id}`);
    }
  }, [saveSuccess, newDocument]);

  if (isSaving) {
    return <Paragraph>Saving...</Paragraph>;
  }

  if (saveSuccess) {
    return <Paragraph>Saved!</Paragraph>;
  }

  return (
    <>
      <h1>New document</h1>

      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(formOnSubmit)}>
          <FlexContainer gap={0.5}>
            <FlexChild grow={1}>
              <SelectField fieldName="type" required>
                <option value="simple">Simple</option>
                <option value="custom">Custom</option>
                <option value="advanced">Advanced</option>
              </SelectField>
            </FlexChild>
            <FlexChild grow={1}>
              <InputField fieldName="date" type="date" required />
            </FlexChild>
          </FlexContainer>
          <InputField fieldName="title" type="text" required />
          {(watchType === 'custom' || watchType === 'advanced') && (
            <InputField fieldName="text" type="textarea" required />
          )}
          {watchType === 'advanced' && (
            <InputField fieldName="images" type="file" required />
          )}
          <Paragraph align="right">
            <Button type="submit" subTheme="success">
              Save Document
            </Button>
            <ButtonLink subTheme="warning" to="/">
              Cancel & Return
            </ButtonLink>
          </Paragraph>
        </form>
      </FormProvider>
    </>
  );
}
