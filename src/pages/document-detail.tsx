import React, { useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';

import { SerializedError } from '@reduxjs/toolkit';
import { FetchBaseQueryError } from '@reduxjs/toolkit/dist/query';

import DocumentDetailBottomBar from '../components/document-detail/bottom-bar';
import { Paragraph, Pre } from '../components/styled/base-components';
import {
    DocumentsListLink, HeaderBar, ImageContainer, NextDocumentLink, PrevDocumentLink
} from '../components/styled/document-detail';
import { useDeleteDocumentMutation, useGetDocumentQuery } from '../services/documents-api';

const isFetchBaseQueryError = (
  error: FetchBaseQueryError | SerializedError
): error is FetchBaseQueryError => {
  return (error as FetchBaseQueryError).status !== undefined;
};

export default function DocumentDetailPage() {
  const { id } = useParams();

  if (!id) {
    return <Paragraph>You must specify a document Id.</Paragraph>;
  }

  const navigate = useNavigate();
  const {
    data,
    isFetching,
    error: loadError,
  } = useGetDocumentQuery(id, { refetchOnMountOrArgChange: true });
  const [deleteDocument, { isLoading: isDeleting, isSuccess: deleteSuccess }] =
    useDeleteDocumentMutation();

  useEffect(() => {
    if (isFetching) {
      document.title = `Loading document ${id}`;
    } else if (loadError) {
      document.title = `Error loading document ${id}`;
    } else if (data) {
      document.title = `Document details for: ${data.document.title}`;
    }
  }, [isFetching, data, loadError]);

  useEffect(() => {
    if (deleteSuccess) {
      navigate('/');
    }
  }, [deleteSuccess]);

  if (loadError) {
    if (isFetchBaseQueryError(loadError)) {
      if (loadError.status === 404) {
        return <Paragraph>Document not found</Paragraph>;
      }

      return (
        <Paragraph>
          Error {loadError.status} while loading the Document.
        </Paragraph>
      );
    }
    return <Paragraph>Could not load the document</Paragraph>;
  }

  if (isFetching) {
    return <Paragraph>Loading the document</Paragraph>;
  }

  if (isDeleting) {
    return <Paragraph>Deleting the document</Paragraph>;
  }

  if (data) {
    return (
      <>
        <HeaderBar>
          {data.prevDocumentId && (
            <PrevDocumentLink>
              <Link to={`/documents/${data.prevDocumentId}`}>
                Previous document
              </Link>
            </PrevDocumentLink>
          )}
          <DocumentsListLink>
            <Link to="/">Return to document List</Link>
          </DocumentsListLink>
          {data.nextDocumentId && (
            <NextDocumentLink>
              <Link to={`/documents/${data.nextDocumentId}`}>
                Next document
              </Link>
            </NextDocumentLink>
          )}
        </HeaderBar>

        <h1>{data.document.title}</h1>
        {data.document.text && <Pre>{data.document.text}</Pre>}
        {data.document.image && (
          <Paragraph align="center">
            <ImageContainer
              src={data.document.image}
              alt={data.document.title}
            />
          </Paragraph>
        )}
        <DocumentDetailBottomBar
          document={data.document}
          deleteButtonOnClick={deleteDocument}
        />
      </>
    );
  }

  return null;
}
