import React from 'react';
import { Provider } from 'react-redux';

import { render } from '@testing-library/react';

import { store } from '../app/store';
import DocumentsPage from './documents';

jest.mock('react-router-dom', () => ({
  useSearchParams() {
    return [
      {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        get: () => {},
      },
    ];
  },
}));

test('renders title', () => {
  const { getByText } = render(
    <Provider store={store}>
      <DocumentsPage />
    </Provider>
  );

  expect(getByText(/available documents/i)).toBeInTheDocument();
});
