import React from 'react';
import JSONPretty from 'react-json-pretty';
import { useSearchParams } from 'react-router-dom';

import DocumentTypesSelect from '../components/document-types-select';
import DocumentsList from '../components/documents-list';
import Paginator from '../components/paginator';
import { ButtonLink, Paragraph } from '../components/styled/base-components';
import { IDocumentsQuery } from '../interfaces/documents-query';
import { useGetDocumentsQuery } from '../services/documents-api';
import { isTDocumentType, TDocumentType } from '../types/document-type';

export default function DocumentsPage() {
  const [searchParams, setSearchParams] = useSearchParams();
  const offset = parseInt(searchParams.get('offset') ?? '0', 10);
  const limit = parseInt(searchParams.get('limit') ?? '10', 10);
  const filter = (searchParams.get('filter') ?? 'all') as TDocumentType;

  const queryParams: IDocumentsQuery = {
    offset,
    limit,
  };

  if (isTDocumentType(filter)) {
    queryParams.filter = filter;
  }

  const { data, error, isFetching } = useGetDocumentsQuery(queryParams);

  const documentTypesSelectOnChange = (type: TDocumentType) =>
    setSearchParams({ filter: type });

  return (
    <>
      <DocumentTypesSelect
        value={isTDocumentType(filter) ? filter : 'all'}
        onChange={documentTypesSelectOnChange}
      />
      {isFetching && <p>Refreshing list...</p>}
      {error && (
        <>
          <p>An error has ocurred retrieving the documents list</p>
          <JSONPretty data={error} />
        </>
      )}
      {!isFetching && data && (
        <>
          <DocumentsList documents={data.documents} />
          <Paginator
            {...{
              total: data.count,
              offset,
              limit,
              filter,
            }}
          />
        </>
      )}
      <Paragraph align="center">
        <ButtonLink subTheme="success" to="/documents/new">
          New Document
        </ButtonLink>
      </Paragraph>
    </>
  );
}
