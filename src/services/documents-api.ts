import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

import { IDocument } from '../interfaces/document';
import { IDocumentDetailResponse } from '../interfaces/document-detail-response';
import { IDocumentsQuery } from '../interfaces/documents-query';
import { IDocumentsResponse } from '../interfaces/documents-response';

export const documentsEndPoint = 'documents';
export const baseUrl = '/api/';
export const documentsApi = createApi({
  reducerPath: 'documentsApi',
  baseQuery: fetchBaseQuery({ baseUrl }),
  tagTypes: ['Documents'],
  endpoints: (builder) => ({
    getDocuments: builder.query<IDocumentsResponse, IDocumentsQuery>({
      query(params) {
        return {
          url: documentsEndPoint,
          params,
        };
      },
      providesTags: (result): { type: 'Documents'; id: string }[] =>
        result
          ? [
              ...result.documents.map<{ type: 'Documents'; id: string }>(
                ({ id }) => ({ type: 'Documents', id })
              ),
              { type: 'Documents', id: 'LIST' },
            ]
          : [{ type: 'Documents', id: 'LIST' }],
    }),
    getDocument: builder.query<IDocumentDetailResponse, string>({
      query: (id) => `${documentsEndPoint}/${id}`,
      providesTags: (result, error, id) => [{ type: 'Documents', id }],
    }),
    addDocument: builder.mutation<IDocument, Omit<IDocument, 'id'>>({
      query: (document) => ({
        url: documentsEndPoint,
        method: 'POST',
        body: document,
      }),
      invalidatesTags: [{ type: 'Documents', id: 'LIST' }],
    }),
    deleteDocument: builder.mutation<void, string>({
      query: (id) => ({
        url: `${documentsEndPoint}/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: (result, error, id) => [{ type: 'Documents', id }],
    }),
  }),
});

export const {
  useGetDocumentsQuery,
  useGetDocumentQuery,
  useAddDocumentMutation,
  useDeleteDocumentMutation,
} = documentsApi;
