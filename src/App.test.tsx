import React from 'react';
import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { store } from './app/store';
import App from './App';
import { MemoryRouter } from 'react-router-dom';

test('renders document list', () => {
  const { getByText } = render(
    <Provider store={store}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </Provider>
  );

  expect(getByText(/available documents/i)).toBeInTheDocument();
});

test('renders document details', () => {
  const i = 1;
  const { getByText } = render(
    <Provider store={store}>
      <MemoryRouter initialEntries={[`/documents/${i}`]}>
        <App />
      </MemoryRouter>
    </Provider>
  );

  expect(getByText(i)).toBeInTheDocument();
});
