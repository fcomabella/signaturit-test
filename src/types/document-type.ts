export type TDocumentType = 'simple' | 'custom' | 'advanced' | 'all';

export const isTDocumentType = (
  filter: string | null
): filter is TDocumentType => {
  return (
    filter === 'advanced' ||
    filter === 'custom' ||
    filter === 'simple' ||
    filter === 'all' ||
    filter === null
  );
};
