import React from 'react';
import { Route, Routes } from 'react-router-dom';

import { CenteredContent } from './components/styled/base-components';
import DocumentDetailPage from './pages/document-detail';
import DocumentsPage from './pages/documents';
import NewDocumentPage from './pages/new-document';
import NotFoundPage from './pages/not-found';

function App() {
  return (
    <CenteredContent>
      <Routes>
        <Route path="/" element={<DocumentsPage />} />
        <Route path="/documents/new" element={<NewDocumentPage />} />
        <Route path="/documents/:id" element={<DocumentDetailPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </CenteredContent>
  );
}

export default App;
